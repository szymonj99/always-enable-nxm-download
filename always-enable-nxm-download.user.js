// ==UserScript==
// @name           Enable “Mod manager download” for all files
// @name:da        Slå "Mod manager download" til for alle filer
// @namespace      https://freso.dk
// @version        2019.6.12.1
// @author         Frederik “Freso” S. Olesen
// @license        GPL-3.0-or-later
// @description    Enable the “Mod manager download” button for all files on the “Files” tabs on NexusMods.com mod pages.
// @description:da Slå "Mod manager download"‐knappen til for alle filer under "Files"‐fanen på NexusMods.com modsider.
// @homepageURL    https://gitlab.com/Freso/always-enable-nxm-download
// @icon           https://www.nexusmods.com/Contents/Images/favicons/favicon_ReskinOrange/favicon-32x32.png
// @updateURL      https://gitlab.com/Freso/always-enable-nxm-download/raw/master/always-enable-nxm-download.user.js
// @downloadURL    https://gitlab.com/Freso/always-enable-nxm-download/raw/master/always-enable-nxm-download.user.js
// @supportURL     https://gitlab.com/Freso/always-enable-nxm-download/issues
// @include        /^https?://(www\.)?nexusmods\.com/[a-z0-9]+/mods/[0-9]+.*$/
// @grant          none
// ==/UserScript==

function generate_nxm_url(file_href, mod_href) {
  //console.log('generate_nxm_url: Start!');

  var http_url = new URL(file_href);
  var fileId = http_url.searchParams.get('id');

  var mod_url = new URL(mod_href);
  var mod_url_path_parts = mod_url.pathname.split('/').slice(1);
  var gameId = mod_url_path_parts[0];
  //Allows the JS script to work out of the box with default configurations of NXM Handler in MO2.
  if (gameId == "skyrimspecialedition")
    gameId = "skyrimse";
  var modId = mod_url_path_parts[2];
  
  console.log('Game ID: ' + gameId);
  console.log('Mod ID : ' + modId);
  console.log('File ID: ' + fileId);

  var nxm_url = new URL('nxm://' + gameId + '/mods/' + modId + '/files/' + fileId);
  
  //console.log('generate_nxm_url: Stop!');
  return nxm_url;
}

function manual_to_manager(file_link, mod_link) {
  //console.log('manual_to_manager: Start!');

  // Turn manual link into mod manager link
  file_link.href = generate_nxm_url(file_link.href, mod_link.href);

  // Change SVG icon
  var icon = file_link.querySelector('svg.icon-manual');
  icon.classList.replace('icon-manual', 'icon-nmm');
  var icon_href = icon.querySelector('use').attributes['xlink:href'];
  var new_icon_url = new URL(icon_href.value);
  new_icon_url.hash = 'icon-nmm';
  icon_href.value = new_icon_url;

  // Change link/button text
  var link_span = file_link.querySelector('span.flex-label');
  link_span.textContent = 'nxm:// download';

  //console.log('manual_to_manager: Stop!');
}

// Callback function to execute when mutations are observed
var callback = function(mutationsList, observer) {
  //console.log(mutationsList);
  var files_tab = document.querySelector('div#mod_files');
  //console.log(files_tab);
  if (files_tab) {
    console.log('It’s the Files tab!');
    if (files_tab.querySelectorAll('ul.accordion-downloads svg.icon-nmm').length == files_tab.querySelectorAll('ul.accordion-downloads svg.icon-manual').length) {
      console.log('Mod manager download already enabled for all files. Nothing else to do.');
    } else {
      console.log('Mod manager downloads are NOT enabled for all files!');

      var mod_files = files_tab.querySelectorAll('ul.accordion-downloads');

      mod_files.forEach(function(mod_file) {
        //console.log(mod_file);
        if (mod_file.querySelector('svg.icon-nmm')) {
          console.log('Mod manager download already enabled for this file. Continuing…');
        } else {
          console.log('Only manual downloads for this file. Inserting mod manager download link…');

          var linkManualDownload = mod_file.querySelector('a > svg.icon-manual').parentElement;
          var linkManagerDownloadListItem = linkManualDownload.parentElement.cloneNode(true);
          var linkManagerDownload = manual_to_manager(linkManagerDownloadListItem.querySelector('a'), window.location);

          // Insert "Mod manager download" button into list
          mod_file.insertBefore(linkManagerDownloadListItem, linkManualDownload.parentElement);
        }
      });
    }
  } else {
    console.log('Not the Files tab.');
  }
};

// Create an observer instance linked to the callback function
var observer = new MutationObserver(callback);

// Start observing the target node for configured mutations
observer.observe(document.querySelector('div.tabcontent'), { attributes: false, childList: true, subtree: true });
